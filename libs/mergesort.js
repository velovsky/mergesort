var mergeSort =
{
	array: [],
	main: function()
	{		
		//array
		mergeSort.array = manager.queue;

		if(mergeSort.array.length <= 1)
			return;

		//indexes
		low_index = 0;
		high_index = mergeSort.array.length - 1;
		
		//run mergeSort
		mergeSort.init(mergeSort.array,low_index,high_index);
		
		manager.output();
	},
  init: function(array_a,low_i,high_i)
  {
    // array_b = array_a; //buffer
    mergeSort.split(array_a,low_i,high_i);
  },
  split: function(array_a,low_i,high_i)
  {
    if(high_i - low_i < 1)
      return;
    
    var middle_i = Math.floor((high_i + low_i)/2);
    
    //split first half
    mergeSort.split(array_a,low_i,middle_i);
    //split second half
    mergeSort.split(array_a,middle_i+1,high_i);
    
    //sort
    mergeSort.sort(array_a,low_i,middle_i,high_i);
  },
  sort: function(array_a,low_i,middle_i,high_i)
  {
    var i = low_i;
    var j = middle_i;
    
    //left array buffer
    var left_arr = [];
    var count = 0;
    for(var i = low_i; i <= middle_i; i++)
    {
      left_arr[count] = array_a[i];
      count++;
    }
    
    //right array buffer
    var right_arr = [];
    count = 0;
    for(var i = middle_i+1; i <= high_i; i++)
    {
      right_arr[count] = array_a[i];
      count++;
    }
    
    var i = 0;
    var j = 0;
    var k = low_i;
    while( (i < left_arr.length) && (j < right_arr.length) )
    {
      if(left_arr[i] <= right_arr[j])
      {
        array_a[k] = left_arr[i];
        i++; //next
      }
      else
      {
        array_a[k] = right_arr[j];
        j++; //next
      }
      k++; //next pos in main array
    }
    
    if(j === right_arr.length) //j > index of last element
    {
      //fill remaining from left array
      while(k <= high_i)
      {
        array_a[k] = left_arr[i];
        i++;
        k++;
      }
    }
      
  }
  	
}